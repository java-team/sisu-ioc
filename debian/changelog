sisu-ioc (2.3.0-11) unstable; urgency=medium

  * Team upload.
  * Depend on libplexus-classworlds-java instead of libplexus-classworlds2-java
  * Suggest instead of recommending the optional dependencies
  * Removed the unused build dependency on libmaven-install-plugin-java
  * Standards-Version updated to 4.1.1

 -- Emmanuel Bourg <ebourg@apache.org>  Sat, 11 Nov 2017 01:01:56 +0100

sisu-ioc (2.3.0-10) unstable; urgency=medium

  * Team upload.
  * Depend on libplexus-component-annotations-java
    instead of libplexus-containers1.5-java
  * Standards-Version updated to 4.1.0
  * Switch to debhelper level 10

 -- Emmanuel Bourg <ebourg@apache.org>  Wed, 06 Sep 2017 11:55:48 +0200

sisu-ioc (2.3.0-9) unstable; urgency=medium

  * Team upload.
  * Sort the classes by name in META-INF/sisu/javax.inject.Named to make
    the output reproducible
  * Standards-Version updated to 3.9.8
  * Use secure Vcs-* URLs

 -- Emmanuel Bourg <ebourg@apache.org>  Wed, 31 Aug 2016 10:27:22 +0200

sisu-ioc (2.3.0-8) unstable; urgency=medium

  * Team upload.
  * Replaced guice_classifier.diff with a Maven rule
  * Ignore the project-info-reports plugin to remove a warning with Maven 3
  * Fixed the compilation errors with the tests
  * Adjusted the bundle plugin configuration to still embed the dependencies
    in sisu-inject-bean

 -- Emmanuel Bourg <ebourg@apache.org>  Thu, 17 Dec 2015 13:54:46 +0100

sisu-ioc (2.3.0-7) unstable; urgency=medium

  * Team upload.
  * Fixed the source compatibility with plexus-utils 3.0.22 (Closes: #806486)
  * Fixed a build failure related to the bundle plugin (caused by bnd 2?)
  * Build with the DH sequencer instead of CDBS

 -- Emmanuel Bourg <ebourg@apache.org>  Thu, 10 Dec 2015 13:34:25 +0100

sisu-ioc (2.3.0-6) unstable; urgency=medium

  * Team upload.
  * Removed the unused build dependency on maven-project-info-reports-plugin
  * Removed the unused build dependencies on the *-java-doc packages
  * Removed the unused debian/libsisu-ioc-java-doc.* files
  * Updated Standards-Version to 3.9.6 (no changes)
  * Fixed debian/watch

 -- Emmanuel Bourg <ebourg@apache.org>  Mon, 02 Nov 2015 15:41:48 +0100

sisu-ioc (2.3.0-5) unstable; urgency=medium

  * Team upload.
  * Added patch to address FTBFS issue (Closes: #737591)
  * Updated Standards-Version to 3.9.5 (no changes)

 -- Eugene Zhukov <jevgeni.zh@gmail.com>  Wed, 12 Feb 2014 11:55:37 +0000

sisu-ioc (2.3.0-4) unstable; urgency=low

  * Team upload.

  [ Emmanuel Bourg ]
  * debian/control:
    - Use canonical URLs for the Vcs-* fields
    - Updated Standards-Version to 3.9.4 (no changes)
  * debian/copyright: Merged the duplicate Copyright fields
  * Updated the override for the Lintian codeless-jar warning
  * Removed the now unnecessary patch testng_classifier.diff
  * Build depend on debhelper >= 9
  * Ignore the sisu-eclipse-registry module with a flag in
    libsisu-ioc-java.poms instead of a patch
  * Use XZ compression for the upstream tarball

  [ Eugene Zhukov ]
  * Fix testng version handling in d/maven.rules

 -- Eugene Zhukov <jevgeni.zh@gmail.com>  Fri, 04 Oct 2013 07:52:09 +0000

sisu-ioc (2.3.0-3) unstable; urgency=low

  * Fix FTBFS (LP: #935445) (Closes: #662789):
    - d/maven.rules: Map plexus-component-annotations to version 1.5.5.
    - Patch from James Page. Thanks!
  * d/libsisu-ioc-java.poms: Added --java-lib to ensure jar files still get
    installed to /usr/share/java.
  * Bump Standards-Version to 3.9.3.1: no changes needed.
  * d/copyright: Use copyright-format 1.0.

 -- Damien Raude-Morvan <drazzib@debian.org>  Wed, 07 Mar 2012 19:41:20 +0100

sisu-ioc (2.3.0-2) unstable; urgency=low

  * Add explicit Build-Depends on testng since libsurefire-java doesn't
    Depends on it anymore. (Closes: #655813)

 -- Damien Raude-Morvan <drazzib@debian.org>  Sat, 14 Jan 2012 21:13:05 +0100

sisu-ioc (2.3.0-1) unstable; urgency=low

  * Initial release (Closes: #648657)

 -- Damien Raude-Morvan <drazzib@debian.org>  Sat, 10 Dec 2011 00:45:03 +0100
